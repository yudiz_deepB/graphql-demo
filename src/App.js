import "./App.css";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  split,
  createHttpLink,
} from "@apollo/client";
import User from "./Component/User";
import Form from "./Component/Form";
import { WebSocketLink } from "@apollo/client/link/ws";
import { getMainDefinition } from "@apollo/client/utilities";




const httpLink = createHttpLink({
  uri: "https://apollo-graphql-chat-app.herokuapp.com/graphql",
});

const wsLink = new WebSocketLink({
  uri: "wss://apollo-graphql-chat-app.herokuapp.com/graphql",
  options: {
    reconnect: true,
  },
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  httpLink
);


export const client = new ApolloClient({
  link: splitLink,
  cache: new InMemoryCache(),
  connectToDevTools: true,
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Form />
      <User />
    </ApolloProvider>
  );
}

export default App;
