import { gql } from "@apollo/client";

export const NEW_USER = gql`
  subscription NewUser {
    newUser {
      id
      name
      email
    }
  }
`;
