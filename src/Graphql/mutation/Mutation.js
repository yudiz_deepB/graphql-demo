import { gql } from "@apollo/client";

export const DELETE_USER_MUTATION = gql`
  mutation DeleteUserMutation($deleteUserEmail: String!) {
    deleteUser(email: $deleteUserEmail)
  }
`;

export const CREATE_USER_MUTATION = gql`
  mutation CreateUserMutation(
    $createUserName: String!
    $createUserEmail: String!
  ) {
    createUser(name: $createUserName, email: $createUserEmail) {
      id
      name
      email
    }
  }
`;
