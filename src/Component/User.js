import { useMutation, useQuery, useSubscription } from "@apollo/client";
import React from 'react'
import { GET_USERS } from '../Graphql/queries/Queries';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Container } from "@mui/material";
import Button from '@mui/material/Button';
import { DELETE_USER_MUTATION } from "../Graphql/mutation/Mutation";




const User = () => {
    const {
        loading: UsersLoading,
        data: usersData,
        error: usersError,
    } = useQuery(GET_USERS);

    const [deleteUser] = useMutation(DELETE_USER_MUTATION);

    const handleDelete = (e, email) => {
        e.preventDefault();
        deleteUser({
            variables: {
                deleteUserEmail: email,
            },
            refetchQueries: [GET_USERS],
        });

    }

    return (
        <Container>
            <TableContainer component={Paper} mt={2} pt={2}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow style={{ backgroundColor: '#757de8' }}>
                            <TableCell align="center">NAME</TableCell>
                            <TableCell align="center">EMAIL</TableCell>
                            <TableCell align="center">ACTIONS</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {usersData?.users?.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell align="center" >{row.name}</TableCell>
                                <TableCell align="center">{row.email}</TableCell>
                                <TableCell align="center"><Button onClick={(e) => handleDelete(e, row.email)}>Delete</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container >
    )
}

export default User
