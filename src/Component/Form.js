import React, { useState } from 'react'
import TextField from '@mui/material/TextField';
import { Button, Container } from '@mui/material';
import { useMutation, useSubscription } from "@apollo/client";
import { CREATE_USER_MUTATION } from "../Graphql/mutation/Mutation";
import { GET_USERS } from '../Graphql/queries/Queries';
import { NEW_USER } from '../Graphql/subscription/Subscription';


const Form = () => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");

    const [addUser] = useMutation(CREATE_USER_MUTATION);
    const { data: dataOfSubScription } = useSubscription(NEW_USER);

    const handleAddUser = async (e) => {
        e.preventDefault();
        await addUser({
            variables: {
                createUserName: name,
                createUserEmail: email,
            },
            refetchQueries: [GET_USERS],
        });
        setName("");
        setEmail("");
    };

    return (
        <div className="form">
            <Container>
                <TextField id="outlined-basic" label="NAME" variant="outlined" onChange={(e) => setName(e.target.value)} margin="normal" />
                <br />
                <TextField id="outlined-basic" label="EMAIL" variant="outlined" onChange={(e) => setEmail(e.target.value)} margin="normal" />
                <br />
                <Button onClick={(e) => handleAddUser(e)} className="Btn" variant="outlined" style={{ marginLeft: "60px", marginTop: "10px" }}>Add User</Button>

                {dataOfSubScription &&
                    <h2>{dataOfSubScription?.newUser.name} DATA ADDED</h2>
                }
            </Container>
        </div>
    )
}

export default Form
